"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _core = _interopRequireDefault(require("@better-scroll/core"));

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getChildUI(children, renderProps) {
  // React Element or null
  if (children == null || Array.isArray(children) || /*#__PURE__*/_react["default"].isValidElement(children)) {
    return children;
  } // React Component


  if (children.prototype && children.prototype.isReactComponent) {
    return /*#__PURE__*/_react["default"].createElement(children, renderProps);
  } // Render props


  if (typeof children === 'function') {
    return children(renderProps);
  }

  throw new Error('Invalid children props');
}

var scrollWrapperStyle = {
  position: 'relative',
  overflow: 'hidden',
  height: '100%',
  width: '100%'
};
var scrollWrapperStyleScrollX = {
  display: 'flex',
  alignItems: 'flex-start'
};
var contentStyleScrollX = {
  flex: '1 1 auto'
};
/**
 * @see See [BetterScroll 2.0 Options](https://better-scroll.github.io/docs/en-US/guide/base-scroll-options.html)
 */

var bScrollOptionKeys = ["startX", "startY", "scrollX", "scrollY", "freeScroll", "directionLockThreshold", "eventPassthrough", "click", "dblclick", "tap", "bounce", "bounceTime", "momentum", "momentumLimitTime", "momentumLimitDistance", "swipeTime", "swipeBounceTime", "deceleration", "flickLimitTime", "flickLimitDistance", "resizePolling", "probeType", "preventDefault", "preventDefaultException", "tagException", "HWCompositing", "useTransition", "bindToWrapper", "disableMouse", "disableTouch", "autoBlur", "stopPropagation", "bindToTarget", "autoEndDistance", "outOfBoundaryDampingFactor", "specifiedIndexAsContent"];

var BetterScroll = /*#__PURE__*/_react["default"].forwardRef(function (props, ref) {
  var className = props.className,
      style = props.style,
      addContentNode = props.addContentNode,
      children = props.children,
      plugins = props.plugins;
  var bScrollWrapperRef = (0, _react.useRef)();
  var bScrollRef = (0, _react.useRef)(); // 是否X方向滚动（此时让content的最大宽度突破默认的100%限制）

  var isScrollX = props.scrollX === true && props.scrollY === false; // use BScroll plugins    

  (0, _react.useEffect)(function () {
    (plugins || []).forEach(function (plugin) {
      _core["default"].use(Array.isArray(plugin) ? plugin[0] : plugin);
    });
  }, [plugins]); // Init bScroll

  var initBScrollDeps = bScrollOptionKeys.map(function (key) {
    return props[key];
  });
  initBScrollDeps.push(plugins);
  (0, _react.useEffect)(function () {
    // 先解析plugin配置 
    var bScrollOption = (plugins || []).reduce(function (bScrollOption, plugin) {
      if (Array.isArray(plugin) && plugin[1]) {
        bScrollOption = Object.assign(bScrollOption, plugin[1]);
      }

      return bScrollOption;
    }, {}); // 解析core配置，不能直接赋值undefined，这样BScroll会视为配置值就是undefined,而不走默认值逻辑

    bScrollOptionKeys.reduce(function (bScrollOption, key) {
      if (props[key] != null) {
        bScrollOption[key] = props[key];
      }

      return bScrollOption;
    }, bScrollOption);
    bScrollRef.current = new _core["default"](bScrollWrapperRef.current, bScrollOption);

    if (ref) {
      ref.current = bScrollRef.current;
    }

    return function () {
      bScrollRef.current.destroy();
    };
  }, initBScrollDeps);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: className,
    style: Object.assign({}, scrollWrapperStyle, isScrollX ? scrollWrapperStyleScrollX : null, style),
    ref: bScrollWrapperRef
  }, !addContentNode ? getChildUI(children, {
    scroll: bScrollRef
  }) : /*#__PURE__*/_react["default"].createElement("div", {
    style: isScrollX ? contentStyleScrollX : void 0
  }, getChildUI(children, {
    scroll: bScrollRef
  })));
});

BetterScroll.defaultProps = {
  /** 是否给滚动内容，增加个`div`节点，处理`children`是数组的场景 */
  addContentNode: true
};

var _default = /*#__PURE__*/_react["default"].memo(BetterScroll);

exports["default"] = _default;