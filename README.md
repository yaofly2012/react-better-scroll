# better-scroll-react

让[better-scroll](https://better-scroll.github.io/docs/zh-CN/)的使用更简单。

```jsx
import BetterScroll from 'better-scroll-react'

function Index() {
	return (
		<>
			<div style={{height: 400, border: '1px solid'}}>
				<BetterScroll>
					<div style={{
						height: 600,
						background: 'red'
					}}>
						Hello better-scroll-react
					</div>
				</BetterScroll>
			</div>
		</>
	)
}
```

**注意**：`better-scroll-react`不是[better-scroll](https://better-scroll.github.io/docs/zh-CN/)的React实现，只是在[better-scroll](https://better-scroll.github.io/docs/zh-CN/)基础上包装了一层，让[better-scroll](https://better-scroll.github.io/docs/zh-CN/)的使用更简单：
1. 可以声明式的使用[better-scroll](https://better-scroll.github.io/docs/zh-CN/)；
2. 内部维护`BetterScroll`实例的创建和销毁；
3. 内部自动创建滚动区域的`wrapper`，再也不用担心[`BetterScroll 初始化了， 但是没法滚动`](https://better-scroll.github.io/docs/zh-CN/guide/#%E6%BB%9A%E5%8A%A8%E5%8E%9F%E7%90%86)。

# 安装

```js
npm install @better-scroll/core better-scroll-react
```

# 使用

```jsx
import BetterScroll from 'better-scroll-react'

function Index() {
	return (
		<>
			<div style={{height: 400, border: '1px solid'}}>
				<BetterScroll>
					<div style={{
						height: 600,
						background: 'red'
					}}>
						Hello better-scroll-react
					</div>
				</BetterScroll>
			</div>
		</>
	)
}
```
`better-scroll-react`组件的属性以及属性默认值逻辑同[`better-scroll 配置项`](https://better-scroll.github.io/docs/zh-CN/guide/base-scroll-options.html)

## 获取`BetterScroll`实例

通过`ref`可以获取`BetterScroll`实例

```jsx
import BetterScroll from 'better-scroll-react'
import { useEffect, useRef } from 'react'


function Index() {
	const scrollRef = useRef();
	useEffect(() => {
		scrollRef.current.on('scrollStart', () => {
			console.log('Begin scroll')
		})
	}, [])

	return (
		<>
			<div style={{height: 400, border: '1px solid'}}>
				<BetterScroll ref={scrollRef}>
					<div style={{
						height: 600,
						background: 'red'
					}}>
						Hello better-scroll-react
					</div>
				</BetterScroll>
			</div>
		</>
	)
}
```

## 插件使用

插件使用方式同[better-scroll](https://better-scroll.github.io/docs/zh-CN/plugins/)。

```jsx
import BetterScroll from 'better-scroll-react'
import Pullup from '@better-scroll/pull-up'
import { useEffect, useRef } from 'react'

const plugins= [
    // 插件和插件配置
	[Pullup, { pullUpLoad: true }]
]

function Index() {
	const scrollRef = useRef();

	useEffect(() => {
		scrollRef.current.on('pullingUp', () => {
			console.log('pullingUp')
			setTimeout(() => {
				scrollRef.current.finishPullUp()
			}, 1000)
		})
	}, [])

	return (
		<>
			<div style={{height: 400, border: '1px solid'}}>
				<BetterScroll ref={scrollRef} plugins={plugins}>
					<div style={{
						height: 600,
						background: 'red'
					}}>
						Hello better-scroll-react
					</div>
				</BetterScroll>
			</div>
		</>
	)
}
```
插件的配置属性不能直接传给`better-scroll-react`组件，必须在`plugins`属性里传递（如上面DEMO）

# 属性

|名称|类型|默认值|描述|
|--|--|--|--|
|addContentNode|boolean|true|是否给滚动内容包裹个`div`节点，处理多子节点的场景|
|className|string|无|wrapper容器className|
|style|Object|无|wrapper容器style|
|plugins| Array<>|无|BetterScroll[插件](https://better-scroll.github.io/docs/zh-CN/plugins/)，传入格式：`{plugins: [PullDown]}`, 插件配置数据传入：`{ plugins:[[PullDown, {pullDownRefresh: true}]] }`|
|其他属性同[BetterScroll 2.0 Options](https://better-scroll.github.io/docs/en-US/guide/base-scroll-options.html#startx)||||

# Issues
[让我知道你的问题](https://gitlab.com/yaofly2012/react-better-scroll/issues)