### 基本用法

```jsx
<div style={{height: 400}}>
    <BetterScroll>
        <div style={{
            height: 600,
            background: 'red'
        }}>
            Hello better-scroll-react
        </div>
    </BetterScroll>
</div>
```

### 获取`BetterScroll`实例

通过`ref`可以获取`BetterScroll`实例

```js
import { useEffect, useRef } from 'react'

const scrollRef = useRef();

useEffect(() => {
    scrollRef.current.on('scrollStart', () => {
        // 看console输出
        console.log('Begin scroll')
    })
}, [])

;<div style={{height: 400, border: '1px solid'}}>
    <BetterScroll ref={scrollRef}>
        <div style={{
            height: 600,
            background: 'red'
        }}>
            Hello better-scroll-react
        </div>
    </BetterScroll>
</div>
```

### 插件使用

插件使用方式同[better-scroll](https://better-scroll.github.io/docs/zh-CN/plugins/)。

```jsx
import Pullup from '@better-scroll/pull-up'
import { useEffect, useRef } from 'react'

const scrollRef = useRef();
const plugins= [
    // 插件和插件配置
    [Pullup, { pullUpLoad: true }]
]

useEffect(() => {
    scrollRef.current.on('pullingUp', () => {
        console.log('pullingUp')
        setTimeout(() => {
            scrollRef.current.finishPullUp()
        }, 1000)
    })
}, [])

;<div style={{height: 400, border: '1px solid'}}>
    <BetterScroll ref={scrollRef} plugins={plugins}>
        <div style={{
            height: 600,
            background: 'red'
        }}>
            Hello better-scroll-react
        </div>
    </BetterScroll>
</div>
```

插件的配置属性不能直接传给`better-scroll-react`组件，必须在`plugins`属性里传递（如上面DEMO）