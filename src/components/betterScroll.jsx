import BScroll from "@better-scroll/core";
import React, { useRef, useEffect } from "react";

function getChildUI(children, renderProps) {
    
    // React Element or null
    if(children == null || Array.isArray(children) || React.isValidElement(children)) {
        return children;
    }

    // React Component
    if (children.prototype && children.prototype.isReactComponent) {
        return React.createElement(children, renderProps);
    }

    // Render props
    if (typeof children === 'function') {
        return children(renderProps);
    }

    throw new Error('Invalid children props');
}

const scrollWrapperStyle = {
    position: 'relative',
    overflow: 'hidden',
    height: '100%',
    width: '100%',
}

const scrollWrapperStyleScrollX = {
    display: 'flex',
    alignItems: 'flex-start'
}

const contentStyleScrollX = {
    flex: '1 1 auto'
}

/**
 * @see See [BetterScroll 2.0 Options](https://better-scroll.github.io/docs/en-US/guide/base-scroll-options.html)
 */
const bScrollOptionKeys = [
    "startX",
    "startY",
    "scrollX",
    "scrollY",
    "freeScroll",
    "directionLockThreshold",
    "eventPassthrough",
    "click",
    "dblclick",
    "tap",
    "bounce",
    "bounceTime",
    "momentum",
    "momentumLimitTime",
    "momentumLimitDistance",
    "swipeTime",
    "swipeBounceTime",
    "deceleration",
    "flickLimitTime",
    "flickLimitDistance",
    "resizePolling",
    "probeType",
    "preventDefault",
    "preventDefaultException",
    "tagException",
    "HWCompositing",
    "useTransition",
    "bindToWrapper",
    "disableMouse",
    "disableTouch",
    "autoBlur",
    "stopPropagation",
    "bindToTarget",
    "autoEndDistance",
    "outOfBoundaryDampingFactor",
    "specifiedIndexAsContent"
]

const BetterScroll = React.forwardRef((props, ref) => {
    const { 
        className,
        style,
        addContentNode,
        children,
        plugins
    } = props;

    const bScrollWrapperRef = useRef();
    const bScrollRef = useRef();
    // 是否X方向滚动（此时让content的最大宽度突破默认的100%限制）
    const isScrollX = props.scrollX === true && props.scrollY === false;

    // use BScroll plugins    
    useEffect(() => {
        (plugins || []).forEach(plugin => {            
            BScroll.use(Array.isArray(plugin) ? plugin[0] : plugin);
        })
    }, [plugins]);

    // Init bScroll
    const initBScrollDeps = bScrollOptionKeys.map(key => props[key]);
    initBScrollDeps.push(plugins);    
    
    useEffect(() => { 
        // 先解析plugin配置 
        const bScrollOption = (plugins || []).reduce((bScrollOption, plugin) => {
            if(Array.isArray(plugin) && plugin[1]) {
                bScrollOption = Object.assign(bScrollOption, plugin[1]);
            }
            return bScrollOption;
        }, {});

        // 解析core配置，不能直接赋值undefined，这样BScroll会视为配置值就是undefined,而不走默认值逻辑
        bScrollOptionKeys.reduce((bScrollOption, key) => {        
            if(props[key] != null) {
                bScrollOption[key] = props[key];
            }
            return bScrollOption;
        }, bScrollOption);
        
        bScrollRef.current = new BScroll(bScrollWrapperRef.current, bScrollOption);
        
        if(ref) {
            ref.current = bScrollRef.current;
        }

        return () => {            
            bScrollRef.current.destroy();
        }
    }, initBScrollDeps);

    return (
        <div className={className} 
            style={Object.assign({}, scrollWrapperStyle, isScrollX ? scrollWrapperStyleScrollX : null, style)} 
            ref={bScrollWrapperRef}>
            <Choose>
                <When condition={!addContentNode}>
                    {
                        getChildUI(children, { scroll: bScrollRef })
                    }
                </When>
                <Otherwise>
                    <div style={isScrollX ? contentStyleScrollX : void 0}>
                        {
                            getChildUI(children, { scroll: bScrollRef })
                        }
                    </div>
                </Otherwise>
            </Choose>            
        </div>
    )
})

BetterScroll.defaultProps = {
    /** 是否给滚动内容，增加个`div`节点，处理`children`是数组的场景 */
    addContentNode: true
}

export default React.memo(BetterScroll);