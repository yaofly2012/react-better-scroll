# 0.0.2 (2020-09-29)

## Features
- 优化当且水平方向（scrollX）滚动时`scroll content`DOM节点样式，尽量保证自动添加的`scroll content`DOM不影响滚动内容布局。

# 0.0.1 (2020-09-29)

## Bug Fixes
- 修复`npm`包丢失`dist`目录问题