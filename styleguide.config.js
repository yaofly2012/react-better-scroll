module.exports = {
  components: 'src/components/**/*.jsx',

  showSidebar: false,
  exampleMode: 'expand',
  usageMode: 'expand',

  styleguideDir: 'docs'
};
