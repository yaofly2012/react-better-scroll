const path = require('path')

module.exports = {
    entry: {
        index: './src/index'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            // Babel loader will use your project’s babel.config.js
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            // Other loaders that are needed for your components
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
}